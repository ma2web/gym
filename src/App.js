import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./components/login/Login";
import PrivateRoute from "./PrivateRoute";
import Dashboard from "./components/dashboard/Dashboard";
import { Provider } from "react-redux";
import store from "./redux/store";
import { NotificationContainer } from "react-notifications";
import "react-notifications/dist/react-notifications.css";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import Jss from "./jss";

const fontFamily = "myFont";

const theme = createMuiTheme({
  direction: "rtl",
  typography: {
    fontFamily,
    body1: {
      fontFamily,
    },
    body2: {
      fontFamily,
    },
    button: {
      fontFamily,
    },
    h1: {
      fontFamily,
    },
    h2: {
      fontFamily,
    },
    h3: {
      fontFamily,
    },
    h4: {
      fontFamily,
    },
    h5: {
      fontFamily,
    },
    h6: {
      fontFamily,
    },
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Jss>
        <Provider store={store}>
          <NotificationContainer />
          <div className='app'>
            <Router>
              <Switch>
                <Route path='/login' exact component={Login} />
                <PrivateRoute path='/' exact component={Dashboard} />
              </Switch>
            </Router>
          </div>
        </Provider>
      </Jss>
    </ThemeProvider>
  );
}

export default App;
