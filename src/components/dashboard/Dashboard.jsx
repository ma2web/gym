import React from "react";
import PropTypes from "prop-types";
import Navbar from "../navbar/Navbar";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import Users from "../pages/users/Users";
import Posts from "../pages/posts/Posts";
import { Grid } from "@material-ui/core";
import Setting from "../pages/setting/Setting";

const Dashboard = (props) => {
  return (
    <Router>
      <Grid container>
        <Grid item xs={false} md={2} className='navbar-container'>
          <Navbar />
        </Grid>

        <Grid item xs={12} md={10} className='contents-container'>
          <Switch>
            <Route path='/users' component={Users} exact />
            <Route path='/posts' component={Posts} exact />
            <Route path='/settings' component={Setting} exact />
          </Switch>
        </Grid>
      </Grid>
    </Router>
  );
};

Dashboard.propTypes = {};

export default Dashboard;
