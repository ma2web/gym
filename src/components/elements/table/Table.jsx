import React from "react";
import PropTypes from "prop-types";
import { RiSecurePaymentLine } from "react-icons/ri";
import avatar from "../../../assets/img/avatar.png";

const Table = (props) => {
  return (
    <div className='table-container'>
      <table className='ma2web-table'>
        <thead>
          <tr>
            <th className='radif'>
              <span>ردیف</span>
            </th>
            <th>نام و عکس</th>
            <th>وضعیت</th>
            <th>پلن</th>
            <th>شماره تماس</th>
            <th>معرف</th>
            <th>نشان</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style={{ paddingRight: 25 }}>1</td>
            <td>
              <div className='table-img-name'>
                <img src={avatar} alt='avatar' />{" "}
                <span>نام و نام خانوادگی</span>
              </div>
            </td>
            <td>
              <RiSecurePaymentLine />
            </td>
            <td>P1</td>
            <td>۰۹۱۲۳۴۵۶۷۸۹</td>
            <td>۱۱۵۶۷</td>
            <td>
              <span className='neshan'></span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

Table.propTypes = {};

export default Table;
