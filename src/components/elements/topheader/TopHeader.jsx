import React from "react";
import PropTypes from "prop-types";
import { Grid, TextField } from "@material-ui/core";
import { RiPrinterLine } from "react-icons/ri";
import { AiOutlineQuestion } from "react-icons/ai";

const TopHeader = (props) => {
  return (
    <Grid container className='top-header'>
      <Grid item xs={6}>
        <Grid container>
          <Grid item xs={3}>
            <form>
              <TextField
                id='outlined-basic'
                label='جستجوی مشتریان'
                variant='outlined'
                size='small'
              />
            </form>
          </Grid>
          <Grid item xs={1}>
            <RiPrinterLine />
          </Grid>
          <Grid item xs={1}>
            <AiOutlineQuestion />
          </Grid>
        </Grid>
      </Grid>

      <Grid item xs={6} className='ma2web-date'>
        <p>99/03/05</p>
      </Grid>
    </Grid>
  );
};

TopHeader.propTypes = {};

export default TopHeader;
