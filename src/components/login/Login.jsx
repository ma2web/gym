import React, { useState } from "react";
import PropTypes from "prop-types";
import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";

const Login = (props) => {
  const [login_step, set_login_step] = useState(true);
  return (
    <div className='login'>
      <div className='form-container'>
        {login_step ? <LoginForm /> : <RegisterForm />}
      </div>
    </div>
  );
};

Login.propTypes = {};

export default Login;
