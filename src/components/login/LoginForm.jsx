import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { login } from "../../redux/actions/auth";
import { TextField, Button } from "@material-ui/core";

const LoginForm = ({ login, isAuthenticated, auth }) => {
  const [login_info, set_login_info] = useState({
    username: "",
    password: "",
  });

  const onChange = (e) => {
    set_login_info({
      ...login_info,
      [e.target.id]: e.target.value,
    });
  };

  const onSubmit = async (e) => {
    e.preventDefault();

    login(login_info);
  };

  console.log(auth);
  return (
    <form className='login-form' onSubmit={onSubmit}>
      <div className='form-input'>
        <TextField label='نام کاربری' />
      </div>
      <div className='form-input'>
        <TextField label='رمز عبور' />
      </div>
      <div className='form-input'>
        <Button variant='contained' color='primary'>
          ورود
        </Button>
      </div>
    </form>
  );
};

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
  auth: state.auth,
});

export default connect(mapStateToProps, { login })(LoginForm);
