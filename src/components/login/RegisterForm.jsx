import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { register } from "../../redux/actions/auth";
import { Redirect } from "react-router-dom";
import { TextField } from "@material-ui/core";

const RegisterForm = ({ register, isAuthenticated }) => {
  const [register_info, set_register_info] = useState({
    name: "",
    email: "",
    username: "",
    password: "",
  });

  const onChange = (e) => {
    set_register_info({
      ...register_info,
      [e.target.id]: e.target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    console.log(register_info);

    register(register_info);
  };

  console.log(isAuthenticated);

  if (isAuthenticated) {
    return <Redirect to='/' />;
  }

  return (
    <form noValidate autoComplete='off'>
      <TextField id='standard-basic' label='Standard' />
    </form>
  );
};

RegisterForm.propTypes = {
  register: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { register })(RegisterForm);
