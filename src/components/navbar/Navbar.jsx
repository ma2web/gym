import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { logout } from "../../redux/actions/auth";
import { connect } from "react-redux";
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
  ListItemAvatar,
  Avatar,
} from "@material-ui/core";
import avatar from "../../assets/img/avatar.png";

const Navbar = ({ logout }) => {
  return (
    <div className='navbar'>
      <div className='top-menu'>
        <div className='avatar'>
          <img src={avatar} alt='avatar' />
        </div>
        <div className='user-info'>
          <small>باران رحمتی</small>
          <p>کارشناس پشتیبانی</p>
        </div>
      </div>
      <div className='menu-item'>
        <ul component='nav' aria-label='main mailbox folders'>
          <Link to='/users'>
            <li>اول</li>
          </Link>
          <Link to='/posts'>
            <li>دوم</li>
          </Link>
          <Link to='/settings'>
            <li>تنظیمات</li>
          </Link>
          <li onClick={logout}>
            <a href='#'>خروج</a>
          </li>
        </ul>
      </div>
    </div>
  );
};

Navbar.propTypes = {
  logout: PropTypes.func.isRequired,
};

export default connect(null, { logout })(Navbar);
