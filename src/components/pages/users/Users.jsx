import React from "react";
import PropTypes from "prop-types";
import Table from "../../elements/table/Table";
import TopHeader from "../../elements/topheader/TopHeader";

const Users = (props) => {
  return (
    <div>
      <TopHeader />
      <Table />
    </div>
  );
};

Users.propTypes = {};

export default Users;
