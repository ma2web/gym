import React from "react";
import { create } from "jss";
import rtl from "jss-rtl";
import { StylesProvider, jssPreset } from "@material-ui/core/styles";

// Configure JSS to use RTL and Extend Plugins
const jss = create({
  plugins: [...jssPreset().plugins, rtl(), ...jssPreset().plugins],
});

function Jss(props) {
  return <StylesProvider jss={jss}>{props.children}</StylesProvider>;
}

export default Jss;
